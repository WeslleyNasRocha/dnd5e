image: node:16-alpine

workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^chore\(release\).*/ || $CI_COMMIT_TAG
      when: never
    - when: always

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .pnpm-store

before_script:
  - apk update
  - apk add zip curl git openssh-client ack findutils
  - curl -L https://unpkg.com/@pnpm/self-installer | node
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - pnpm config set store-dir .pnpm-store
  - pnpm set verify-store-integrity false
  - pnpm install

stages:
  - sync
  - test
  - build
  - release

sync:
  stage: sync
  script:
    - eval $(ssh-agent -s)
    - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add -
    - xargs -L 1 -a .packs curl --create-dirs -OSs --output-dir packs
    - pnpx fvttp --path packs --babele --use-name --no-items --save packs
    - mv packs/*.json dnd5e_pt-BR/lang/en
    - pnpx jsonlint -q --enforce-double-quotes dnd5e_pt-BR
    - git add dnd5e_pt-BR/lang/en
    - "git commit -m 'chore(sync): update files' || echo 'No changes, nothing to commit!'"
    - git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
    - git push origin HEAD:$CI_COMMIT_REF_NAME
  rules:
      - if: $CI_PIPELINE_SOURCE == 'schedule'

jsonlint:
  stage: test
  script:
    - pnpx jsonlint -q --enforce-double-quotes dnd5e_pt-BR
  rules:
    - if: $CI_PIPELINE_SOURCE != 'schedule'
      changes:
          - dnd5e_pt-BR/**/*.json

semistandard:
  stage: test
  script:
    - pnpx semistandard
  rules:
    - if: $CI_PIPELINE_SOURCE != 'schedule'
      changes:
          - dnd5e_pt-BR/**/*.js

build:
  stage: build
  script:
    - export NEXT_VERSION=$(pnpx semantic-release --dry-run | ack -io '(?<=The next release version is )(.*)(?=$)')
    - export NEXT_DOWNLOAD=$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/dnd5e_pt-BR/$NEXT_VERSION/dnd5e_pt-BR.zip
    - pnpx json -I -f dnd5e_pt-BR/module.json -e 'this.version="'$NEXT_VERSION'"' -e 'this.download="'$NEXT_DOWNLOAD'"'
    - pnpx json -I -f package.json -e 'this.version="'$NEXT_VERSION'"'
    - zip -r -9 dnd5e_pt-BR.zip dnd5e_pt-BR -x "dnd5e_pt-BR/lang/en/*"
  artifacts:
    name: dnd5e_pt-BR
    when: on_success
    paths:
      - package.json
      - dnd5e_pt-BR.zip
      - dnd5e_pt-BR/module.json
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^(master|beta)/ && $CI_PIPELINE_SOURCE != 'schedule' && $CI_COMMIT_MESSAGE =~ /^(feat|fix).*/

publish:
  stage: release
  script:
    - eval $(ssh-agent -s)
    - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add -
    - pnpx semantic-release
  rules:
      - if: $CI_COMMIT_BRANCH =~ /^(master|beta)/ && $CI_PIPELINE_SOURCE != 'schedule' && $CI_COMMIT_MESSAGE =~ /^(feat|fix).*/
